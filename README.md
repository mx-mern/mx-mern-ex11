# Yêu cầu 
Dựa trên database xây dựng menu cho nhà hàng trong buổi 11, 
tổ chức model cho các dữ liệu và bổ sung combo trong menu. Viết các api:
- Get menu của từng nhà hàng.
- Lấy danh sách các món trong nhóm món.
- Lấy danh sách các món trong combo.

# Thực hiện 

STORE >>> CATEGORY >>> ITEM
```js
Store: {
    name: String, // required
    address: String,
    phone: String, 
    description: String,
}

item: {
    name: String,
    price: Number,
    description: String,
    thumnail: filePath
    unit: String
    categoryID: objectID ----> category
    storeID:objectID ---> store
}

category: {
    name: String,
    storeID: objectID ---> store
}

relationships :
    category - item: 1-n
    store - item: 1-n
    store - category: 1-n
```

# Note

# List of API
## STORE
### Create Store
POST: http://localhost:8080/api/store 
``` body
{
	"name": "Ga ran VietNam",
	"address": "47 Nguyen Tuan",
	"phone": "0986899769",
	"description": "Ga ran VietNam is the world best Fried Chicken Brand"
}
```
### Get Store by storeID
GET: http://localhost:8080/api/store 

## CATEGORY
### Creat Category
POST: http://localhost:8080/api/category
```
{
	"name": "Chicken of Burger King",
	"store": "5dff0047b981ff46146c3404"
}
```
### GET Category by category ID
GET: http://localhost:8080/api/category
{
	"category": "5dfc54e11bc5d02d8a40784a"
}

## ITEMS
### CREATE ITEM
POST: http://localhost:8080/api/item
```
{
	"name": "Ga ran (4 mieng) BurgerKing",
	"price": "12000",
	"combo": true,
	"unit": "mieng",
	"category": "5dff02047a73c20e000301bc",  
	"store": "5dff0047b981ff46146c3404"
}
```
### GET ITEM BY ITEM ID
GET: http://localhost:8080/api/item/get
```
{

}
```
### GET ITEM BY STOREID
GET: http://localhost:8080/api/item/itemsByStoreID
```
{
	"store": "5dff0047b981ff46146c3404"
}
```
### GET ITEM BY STORE NAME
GET: http://localhost:8080/api/item/itemsByStoreName
```
{
	"store": "KFC"
}
```
# REFERENCE
- https://stackoverflow.com/questions/45264216/how-to-use-mongoose-lookup-child-match

- Các tài liệu hướng dẫn xây dựng database với mongoDB:

    - https://docs.mongodb.com/guides/server/introduction/
    - Thiết kế model (https://docs.mongodb.com/manual/core/data-model-design/)
    - 6 quy tắc của mongoDB Schema (3 phần) (https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-1)
    - Tài liệu đọc thêm về kiến trúc MongoDB (https://drive.google.com/file/d/104t5TjqwJC_-Rl6jBRBF6CpJHBuS3XP0/view?usp=sharing)