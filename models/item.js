const mongoose = require('mongoose');
const categoryModel = require('./category');
const storeModel = require('./store');
const Schema = mongoose.Schema;



// Define a schema for item 
/*
item: {
    name: String,
    price: Number,
    description: String,
    thumnail: filePath
    unit: String
    categoryID: objectID ----> category
    storeID:objectID ---> store
}
*/
const itemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String
    },
    thumnail: {
        type: String // url or relative path
    },
    unit: {
        type: String,
        required: true
    },
    combo: {
        type: Boolean,
        default: false
    },
    store: {
        type: Schema.Types.ObjectId,
        ref: 'Store',
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
})


// Create a model from that schema
const Item = mongoose.model('Item', itemSchema);

// create a model's map to export
let itemModel = {}

itemModel.createItem = function (newItem, cb) {
    var item = new Item(newItem);
    item.save(function (err, data) {
        cb(err, data);
    })
}

itemModel.getItemById = function (itemId, cb) {
    Item.findById(itemId).populate({
        path: 'category',
        populate: { path: 'store' }
    }).exec(function (err, data) {
        cb(err, data);
    });
}

itemModel.getItemsByCategoryID = function (cat, cb) {
    Item.find(
        {
            category: cat
        }
    ).exec(function (err, data) {
        cb(err, data);
    })
}

itemModel.getItemsbyStoreID = function (storeID, cb) {
    Item.find(
        {
            store: storeID
        }
    ).exec(function (err, data) {
        cb(err, data);
    })
}

itemModel.getItemsbyStoreName = function (storeName, cb) {
    Item.aggregate([
        {
            $lookup: {
                from: 'stores',
                as: 'storeInfo',
                let: {storeID: '$store'},
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    // $id is _id in foreign table,
                                    // $$storeId is local variable assigned in above let statement
                                    {$eq: ['$_id', '$$storeID']},
                                    {$eq: ['$name', storeName]} 
                                ]                                
                            }
                        }
                    }
                ]
            }
        }
        , { $unwind : "$storeInfo" }  // sau khi unwind co' the loai bo nhung storeInfo rong do match de lai
    ]).exec(function (err, dat) {
        cb(err, dat)
    })
}

// itemModel.getItemsbyStoreID = function (storeID, cb) {
//     categoryModel.getCategoriesByStoreID(storeID, function (err, data) {
//         if (err) {
//             cb(err, data);
//         } else {
//             Item.aggregate([
//                 {
//                     $lookup: {
//                         from: 'categories',
//                         pipeline: [
//                            { $match: { store: storeID } }
//                         ],
//                         as: "menus"
//                     }
//                 }
//             ]).exec(function (err, dat) {
//                 cb(err, dat)
//             })
//         }
//     })
// }

module.exports = itemModel;


