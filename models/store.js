const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Define a schema for Store
const storeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    phone: {
        type: String
    }, 
    description: {
        type: String
    }
})

// Create a model from that schema
const Store = mongoose.model('Store',  storeSchema);

// create a model's map to export
let storeModel = {}

storeModel.createStore = function(newStore, cb) {
    var store = new Store(newStore);
    store.save(function(err, data) {
        cb(err, data);
    })
}

storeModel.getStoreById = function(storeId, cb) {
    Store.findById(storeId, function(err, data) {
        cb(err, data);
    });
}

module.exports = storeModel;