const mongoose = require('mongoose');

const Schema = mongoose.Schema;
// Define a schema for category
const categorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  store: {
      type: Schema.Types.ObjectId,
      ref: 'Store',
      required: true
  }
});
// Create a model from that schema (in mongodb)
const Category = mongoose.model('Category', categorySchema);

// Create model export map of functions to export modules after
let categoryModel = {};

categoryModel.createCategory = function(newCat, cb) {
  var newGame = new Category(newCat);
  newGame.save(function(err, data) {
    cb(err, data)
  });
}

categoryModel.getCategoryById = function(catId, cb) {
    Category.findById(catId).populate('store').exec(function(err, data) {
    cb(err, data);
  });
}

categoryModel.getCategoriesByStoreID = function (storeID, cb) {
  Category.find(
      {
          store: storeID
      }
  ).exec(function (err, data) {
      cb(err, data);
  })
}

module.exports = categoryModel;