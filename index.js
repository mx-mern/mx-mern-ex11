const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const store = require('./controllers/store-router');
const category = require('./controllers/category-router');
const item = require('./controllers/item-router');

const app = express();

// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

mongoose.connect('mongodb://localhost:27017/Restaurant', {useNewUrlParser: true});

// connect database
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('DataBase Restaurant Connected');
});


app.use('/api/store', store);
app.use('/api/category', category);
app.use('/api/item', item);

app.listen(8080, function() {
  console.log("Server start on 8080")
});