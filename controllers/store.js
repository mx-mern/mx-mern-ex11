const storeModel = require('../models/store');

let store = {};

store.getStoreById = function(req, res) {

console.log('datreq.params.store_id: ', req.params.store_id);
  storeModel.getStoreById(req.params.store_id, function(err, data) {
    console.log('data: ', data)
    if (err) {
      res.json({
        message: err,
        success: false
      });
    } else if (data) {
      res.json({
        message: 'Success',
        success: true,
        data: data
      });
    } else {
      res.json({
        message: 'store not existed',
        success: true
      });
    }
  });
}

store.createStore = function(req, res) {
  if (req.body.name) {
    storeModel.createStore(req.body, function(err, data) {
      if (err) {
        res.json({
          message: err,
          success: false
        });
      } else {
        res.json({
          message: 'Create store success',
          success: true,
          data: data
        });
      }
    });
  } else {
    res.json({
      message: 'Store\'s name is required',
      success: false
    })
  }
};

module.exports = store;