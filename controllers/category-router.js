const express = require('express');
const categoryController = require('./category');

const router = express.Router();

router.route('/get/:category_id').get(categoryController.getCategoryById);
router.route('/').post(categoryController.createCategory);// create a new category given storeid
router.route('/categories').get(categoryController.getCategoriesByStoreID);

module.exports = router;