const express = require('express');
const storeController = require('./store');

const router = express.Router();

router.route('/:store_id').get(storeController.getStoreById);
router.route('/').post(storeController.createStore);

module.exports = router;