const express = require('express');
const itemController = require('./item');

const router = express.Router();

router.route('/get/:item_id').get(itemController.getItemById);
router.route('/').post(itemController.createItem);
router.route('/items').get(itemController.getItemsByCategoryID);
router.route('/itemsByStoreID').get(itemController.getItemsbyStoreID);
router.route('/itemsByStoreName').get(itemController.getItemsbyStoreName);

module.exports = router;