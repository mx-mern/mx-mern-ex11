const itemModel = require('../models/item');

let item = {};


item.createItem = function (req, res) {
  if (req.body.name) {
    itemModel.createItem(req.body, function (err, data) {
      if (err) {
        res.json({
          message: err,
          success: false
        });
      } else {
        res.json({
          message: 'Create item success',
          success: true,
          data: data
        });
      }
    });
  } else {
    res.json({
      message: 'Item\'s name is required',
      success: false
    })
  }
};

item.getItemById = function (req, res) {

  console.log('datreq.params.item_id: ', req.params.item_id);
  itemModel.getItemById(req.params.item_id, function (err, data) {
    console.log('data: ', data)
    if (err) {
      res.json({
        message: err,
        success: false
      });
    } else if (data) {
      res.json({
        message: 'Success',
        success: true,
        data: data
      });
    } else {
      res.json({
        message: 'item not existed',
        success: true
      });
    }
  });
}

item.getItemsByCategoryID = function (req, res) {
  if (req.body.category) {
    itemModel.getItemsByCategoryID(req.body.category, function (err, data) {
      if (err) {
        res.json({
          message: err.errors,
          success: false
        });
      } else {
        res.json({
          message: 'Success',
          data: data,
          success: true
        });
      }
    })
  }
  else {
    res.json({
      message: 'category (id) is required',
      success: false
    })
  }
}



item.getItemsbyStoreID = function (req, res) {
  if (req.body.store) {
    itemModel.getItemsbyStoreID(req.body.store, function (err, data) {
      if (err) {
        res.json({
          message: err.errors,
          success: false
        });
      } else {
        res.json({
          message: 'Success',
          data: data,
          success: true
        });
      }
    })
  }
  else {
    res.json({
      message: 'store (id) is required',
      success: false
    })
  }
}
item.getItemsbyStoreName = function (req, res) {
  if (req.body.store) {
    itemModel.getItemsbyStoreName(req.body.store, function (err, data) {
      if (err) {
        res.json({
          message: err.errors,
          success: false
        });
      } else {
        res.json({
          message: 'Success',
          data: data,
          success: true
        });
      }
    })
  }
  else {
    res.json({
      message: 'store (name) is required',
      success: false
    })
  }
}

module.exports = item;