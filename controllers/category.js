const categoryModel = require('../models/category');

let category = {};

category.getCategoryById = function(req, res) {

console.log('datreq.params.category_id: ', req.params.category_id);
  categoryModel.getCategoryById(req.params.category_id, function(err, data) {
    console.log('data: ', data)
    if (err) {
      res.json({
        message: err,
        success: false
      });
    } else if (data) {
      res.json({
        message: 'Success',
        success: true,
        data: data
      });
    } else {
      res.json({
        message: 'category not existed',
        success: true
      });
    }
  });
}

category.createCategory = function(req, res) {
  if (req.body.name) {
    categoryModel.createCategory(req.body, function(err, data) {
      if (err) {
        res.json({
          message: "Create Category Failed",
          errors: err.errors,
          success: false
        });
      } else {
        res.json({
          message: 'Create category success',
          status: true,
          data: data
        });
      }
    });
  } else {
    res.json({
      message: 'Category\'s name is required',
      success: false
    })
  }
};

category.getCategoriesByStoreID = function (req, res) {
    if (req.body.store) {
        categoryModel.getCategoriesByStoreID(req.body.store, function (err, data) {
        if (err) {
          res.json({
            message: err.errors,
            success: false
          });
        } else {
          res.json({
            message: 'Success',
            data: data,
            success: true
          });
        }
      })
    }
    else {
      res.json({
        message: 'category (id) is required',
        success: false
      })
    }
  }

module.exports = category;